---
layout: post
category: agile
date: '2018-07-29 12:05'
modified: '2018-07-29 12:05'
tags: 'practice, agile, engineering'
title: Hacker's Guide to Engineering
---

Programmers like to disassociate themselves from
_engineering_. Sometimes they do it when moralizingly
lecturing their peers on issues of software quality and
reliability. But at times also in a dismissive way, when
condemning working modes as BUD (big upfront design), or,
and that is a thing of the agilists, differentiating
themselves from non-agile waterfall.

Since then, I have worked with actual engineers (with people
who actually had engineering degrees) and luckily enough got
some training in _classical_ engineering methodology, that I
know more about enginnering than I did as a junior software
person. And what I learned is, that both camps, programmers
who admire and programmers who dismiss classical
engineering, often have no complete picture what engineering
is about. As I learned, it neither is this fantasy-land of
pure quality development, nor a waterfall-wasteland where
architecture astronauts fight the customer.


# Not jumping to solutions

For centuries, engineering has involved not only
engineer(s), but also a customer, who paied for engineering.
So all engineering methodologies are very much concerned
with the question of how engineers can make sure, that they
actually provide value to their customers. This may be a bit
counterintuitive at first, because naively we would define
an engineers job to build things. Yet, engineering takes a
step back and looks at the problem as a whole. Customers and
users have problems that need to be solved. So the first
step at solving these problems is to actually understand and
define what the problems are.

It can actually challenging to reveal the true problems.
